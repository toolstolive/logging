﻿using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;
using ToolsToLive.Logging.Sinks;

namespace ToolsToLive.Logging
{
    public static class LoggingConfigurationExtensions
    {
        public static LoggerConfiguration ConfigureLogging(this LoggerConfiguration loggerConfiguration, IConfiguration configuration)
        {
            loggerConfiguration.Enrich.FromLogContext();
            loggerConfiguration.Enrich.WithThreadId();
            loggerConfiguration.Enrich.WithMachineName();

            var appName = configuration.GetValue<string>("LogSettings:Application");
            if (!string.IsNullOrWhiteSpace(appName))
            {
                loggerConfiguration.Enrich.WithProperty("Application", appName);
            }

            var minimumLevel = configuration.GetValue("LogSettings:MinimumLevel:Default", LogEventLevel.Verbose);
            loggerConfiguration.MinimumLevel.Is(minimumLevel);

            var overMinLevel = configuration.GetSection("LogSettings:MinimumLevel:Override");
            foreach (var item in overMinLevel.GetChildren())
            {
                var key = item.Key;
                var val = overMinLevel.GetValue<LogEventLevel>(key);
                loggerConfiguration.MinimumLevel.Override(key, val);
            }

            loggerConfiguration
                .ConfigureConsoleLogger(configuration)
                .ConfigureHttpLogger(configuration)
                .ConfigureFileLogger(configuration);

            return loggerConfiguration;
        }
    }
}
