﻿using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;
using System;

namespace ToolsToLive.Logging.Sinks
{
    internal static class HttpLogger
    {
        public static LoggerConfiguration ConfigureHttpLogger(this LoggerConfiguration loggerConfiguration, IConfiguration configuration)
        {
            var section = configuration.GetSection("LogSettings:WriteToHttp");

            var isEnabled = section.GetValue<bool>("Enabled", false);
            if (!isEnabled)
            {
                return loggerConfiguration;
            }

            var connectionUrl = section.GetValue<string>("Url");
            if (string.IsNullOrWhiteSpace(connectionUrl))
            {
                throw new Exception("Url must be set when logging to http is enabled");
            }

            var restrictedToMinimumLevel = section.GetValue<LogEventLevel>("RestrictedToMinimumLevel", LogEventLevel.Verbose);

            loggerConfiguration.WriteTo.Http(
                requestUri: connectionUrl,
                batchPostingLimit: 500,
                queueLimit: 500, // If more than 500 -- send immediately
                period: TimeSpan.FromSeconds(4), // sending every N seconds
                textFormatter: null,
                batchFormatter: null,
                restrictedToMinimumLevel: restrictedToMinimumLevel,
                httpClient: null);

            return loggerConfiguration;
        }
    }
}
