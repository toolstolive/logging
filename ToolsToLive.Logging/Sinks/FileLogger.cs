﻿using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;
using System;

namespace ToolsToLive.Logging.Sinks
{
    internal static class FileLogger
    {
        public static LoggerConfiguration ConfigureFileLogger(this LoggerConfiguration loggerConfiguration, IConfiguration configuration)
        {
            var section = configuration.GetSection("LogSettings:WriteToFile");

            var isEnabled = section.GetValue<bool>("Enabled", false);
            if (!isEnabled)
            {
                return loggerConfiguration;
            }

            var path = section.GetValue<string>("Path");
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new Exception("Path must be set when logging to file is enabled");
            }

            var restrictedToMinimumLevel = section.GetValue<LogEventLevel>("RestrictedToMinimumLevel", LogEventLevel.Verbose);
            var outputTemplate = section.GetValue<string>("OutputTemplate", "{NewLine}{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Properties}{NewLine}{Exception}{NewLine}");

            loggerConfiguration.WriteTo.File(
                path: path,
                restrictedToMinimumLevel: restrictedToMinimumLevel,
                outputTemplate: outputTemplate,
                buffered: true,
                flushToDiskInterval: TimeSpan.FromSeconds(4),
                rollingInterval: RollingInterval.Day
                );

            return loggerConfiguration;
        }
    }
}
