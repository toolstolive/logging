﻿using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;

namespace ToolsToLive.Logging.Sinks
{
    internal static class ConsoleLogger
    {
        public static LoggerConfiguration ConfigureConsoleLogger(this LoggerConfiguration loggerConfiguration, IConfiguration configuration)
        {
            var section = configuration.GetSection("LogSettings:WriteToConsole");

            var isEnabled = section.GetValue<bool>("Enabled", false);
            if (!isEnabled)
            {
                return loggerConfiguration;
            }

            var restrictedToMinimumLevel = section.GetValue<LogEventLevel>("RestrictedToMinimumLevel", LogEventLevel.Verbose);
            var outputTemplate = section.GetValue<string>("OutputTemplate", "{NewLine}{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Properties}{NewLine}{Exception}{NewLine}");

            loggerConfiguration.WriteTo.Console(
                restrictedToMinimumLevel: restrictedToMinimumLevel,
                outputTemplate: outputTemplate
                );

            return loggerConfiguration;
        }
    }
}
