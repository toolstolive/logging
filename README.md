# ToolsToLive.Logging

## Configuration example

```json
{
  "LogSettings": {
    "Application": "MySolution.MyProj",
    "MinimumLevel": {
      "Default": "Information",
      "Override": {
        "Microsoft": "Warning",
        "Microsoft.EntityFrameworkCore.Database": "Warning",
        "Microsoft.Extensions.Diagnostics.HealthChecks.DefaultHealthCheckService": "Fatal"
      }
    },
    "WriteToFile": {
      "Enabled": true,
      "RestrictedToMinimumLevel": "Verbose",
      "OutputTemplate": "{NewLine}{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Properties}{NewLine}{Exception}{NewLine}"
    },
    "WriteToFile": {
      "Enabled": true,
      "Path": "logfiles/MySolution.MyProj_.log",
      "RestrictedToMinimumLevel": "Verbose",
      "OutputTemplate": "{NewLine}{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Properties}{NewLine}{Exception}{NewLine}"
    },
    "WriteToHttp": {
      "Enabled": false,
      "Url": "http://localhost:5701",
      "RestrictedToMinimumLevel": "Debug"
    }
  }
}
```